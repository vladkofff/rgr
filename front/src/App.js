import React, {useState, useEffect} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/Header';
import Main from './components/Main';

function App() {
  const [user, setUser] = useState(null)
  const [accessToken, setAccessToken] = useState("")
  const [currentStatus, setCurrentStatus] = useState(0)
  const [calendars, setCalendars] = useState([])
  const [calendar, setCalendar] = useState(null)
  const [events, setEvents] = useState([])
  
  const data = {
    user: {
      get: user,
      set: setUser
    },
    status: {
      get: currentStatus,
      set: setCurrentStatus
    },
    calendars: {
      get: calendars,
      set: setCalendars
    },
    calendar: {
      get: calendar,
      set: setCalendar
    },
    events: {
      get: events,
      set: setEvents
    },
    accessToken: {
      get: accessToken,
      set: setAccessToken
    },
    baseUrl: "/api"
  }

  return (
    <div className="App">
        <Header data={data}></Header>
        <Main data={data}></Main>  
    </div>
  );
}

export default App;
