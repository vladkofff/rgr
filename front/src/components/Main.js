import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Main.css';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button, Carousel } from "react-bootstrap";


function Main(props) {
    const baseUrl = props.data.baseUrl;

    const getEvents = async (e) => {
        const calendarId = e.target.id;
        console.log(calendarId)
        props.data.calendar.set(props.data.calendars.get.find(i => i.id == calendarId))
        await fetch(`${baseUrl}/event?calendarId=${calendarId}`).then(res => res.json())
            .then(data => data.events).then(data => {
                console.log(calendarId)
                console.log(data)
                props.data.events.set(data)
            });
    }
    const getUsersCalendars = async () => {
        await fetch(`${baseUrl}/calendar/user`, {
            headers: {
                Authorization: `jwt ${props.data.accessToken.get}`,
                'Content-Type': 'application/json;charset=utf-8'
            }
        }
        ).then(res => res.json())
            .then(data => data.calendars).then(props.data.calendars.set);
    }
    const getAllCalendars = () => {
        console.log(props.data.user.get)
        
        fetch(`${baseUrl}/calendar`).then(res => res.json())
            .then(data => data.calendars).then((res) => {
                console.log(res)
                props.data.calendars.set(res)
            });
    }
    const addCalendar = async (e) => {
        e.preventDefault()
        const name = e.target.name.value;

        console.log(name, props.data.accessToken.get)
        await fetch(`${baseUrl}/calendar`, {
            method: 'POST',
            headers: {
                Authorization: `jwt ${props.data.accessToken.get}`,
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify({ name })
        })

        await getAllCalendars();
    }
    const addEvent = async (e) => {
        e.preventDefault()

        const calendarId = props.data.calendar.get.id;
        const date = e.target.date.value;
        const description = e.target.description.value;
        const link = e.target.link.value;
        await fetch(`${baseUrl}/event`, {
            method: 'POST',
            headers: {
                Authorization: `jwt ${props.data.accessToken.get}`,
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify({ calendarId, date, description, link })
        }
        )

        await getEvents({target: {id: calendarId}});
    }

    return (<div>
        <div class="buttons mt-4">
            <Button type="submit" variant="outline-success" onClick={getAllCalendars}>Get All Calendars</Button>

            {
                (() => {
                    if (props.data.accessToken.get) {
                        return (<Button type="submit" variant="outline-success" onClick={getUsersCalendars}>Get My Calendars</Button>)
                    }
                })()
            }


        </div>

        {
                (() => {
                    if (props.data.accessToken.get) {
                        return (
                        <div className="add_calendar_form mt-4 ml-4">
                            <Form inline onSubmit={addCalendar}>
                                <FormControl type="text" placeholder="Name" name="name" className="mr-sm-2" />
                                <Button type="submit" variant="outline-success">Add Calendar</Button>
                            </Form>

                        </div>)
                    }
                })()
            }


        {
            (() => {

                if (props.data.calendars.get) {
                    return (
                        <div class="calendars">
                            {
                                (() => {
                                    return props.data.calendars.get.map(calendar => {
                                        return (<Button type="submit" variant="outline-success" className="calendar m-4" onClick={getEvents} id={calendar.id}>{calendar.name}</Button>)
                                    })})()
                            }
                        </div>
                    )
                }
            })()
        }


        {
            (() => {
                if (props.data.events.get && props.data.events.get.length) {
                    return (
                        <div class="events">
                            <Carousel className="carousel ml-4" indicators="true">
                                {
                                    props.data.events.get.map(i => ({...i, date: new Date(i.date)})).sort((a, b) => a.date.getTime() - b.date.getTime()).map(event => {
                                        console.log(event);
                                        return (
                                        <Carousel.Item>
                                            <img src={event.link} />
                                            <Carousel.Caption>
                                                
                                                <p>{event.description}</p>
                                            </Carousel.Caption>
                                        </Carousel.Item>)
                                    })

                                }
                            </Carousel>

                        </div>
                    )
                }
            })()
        }
        {
            (() => {
                if (props.data.calendar.get && props.data.calendar.get.userId && props.data.user.get && props.data.user.get.id && props.data.calendar.get.userId === props.data.user.get.id) {
                    return (
                    <Form inline onSubmit={addEvent}>
                        <FormControl type="date" placeholder="Date" name="date" className="mr-sm-2" />
                        <FormControl type="text" placeholder="description" name="description" className="mr-sm-2" />
                        <FormControl type="text" placeholder="link to image" name="link" className="mr-sm-2" />
                        <Button type="submit" variant="outline-success">Add Event</Button>
                    </Form>)
                }
            })()
        }

    </div>)
}

export default Main