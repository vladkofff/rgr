import React, {useState, useEffect} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import {Navbar, Nav, NavDropdown, Form, FormControl, Button} from "react-bootstrap";

import {setCookie, getCookie, eraseCookie} from "../utils/cookieUtils"

function Header (props) {
    const baseUrl = props.data.baseUrl; 
    const [isInvalid, setIsInvalid] = useState(false)

    const setUser = async (accessToken) => {
        await fetch(`${baseUrl}/sign`, {
            headers: {Authorization: `jwt ${accessToken}`}
        }).then(async res => {
            if (res.ok) {
                const data = await res.json();
                props.data.user.set(data);
            } else {
                eraseCookie("accessToken")
                props.data.accessToken.set("");
            }
        })
    }

    if (!props.data.accessToken.get) {
        const accessToken = getCookie("accessToken");
        !!accessToken ? (() => {
            props.data.accessToken.set(accessToken)
            setUser(accessToken)  
        })() : (() => {})()
    }



    const login = async (e) => {
        e.preventDefault()
        try {
            await fetch(`${baseUrl}/sign/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({
                    name: e.target.parentNode.name.value,
                    password: e.target.parentNode.password.value
                })
            }).then(res => {
                if(res.ok) {
                    return res
                } else {
                    throw Error()
                }
            }).then(res => res.json()).then(res => {
                props.data.accessToken.set(res.accessToken)
                setCookie("accessToken", res.accessToken)
                props.data.user.set(res.user)
                console.log(res)  
            }).then(_ => setIsInvalid(false)).catch(e => {
                console.log("test")
                
                setIsInvalid(true)
            })
        } catch (e) {
            console.log("test")
            setIsInvalid(true)
        }


        console.log(isInvalid);        
    }
    
    const register = async (e) => {
        e.preventDefault()
        try {
            await fetch(`${baseUrl}/sign/register`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({
                    name: e.target.parentNode.name.value,
                    password: e.target.parentNode.password.value
                })
            }).then(res => {
                if(res.ok) {
                    return res
                } else {
                    throw Error()
                }
            }).then(res => res.json()).then(res => {
                props.data.accessToken.set(res.accessToken)
                setCookie("accessToken", res.accessToken)
                props.data.user.set(res.user)
                console.log(res)  
            }).then(_ => setIsInvalid(false)).catch(e => {
                console.log("test")
                
                setIsInvalid(true)
            })
        } catch (e) {
            console.log("test")
            setIsInvalid(true)
        }
    }
    
    const logout = (e) => {
        e.preventDefault()
        props.data.accessToken.set("")
        eraseCookie("accessToken")
        props.data.status.set(0)
    }

    return (<Navbar bg="light" expand="lg">
    <Navbar.Brand href="#home">Online-Calendars</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
        
      </Nav>
      {
      
      props.data.accessToken.get ?
       (
        <Form inline>
            <Button type="submit" variant="outline-success" onClick={logout}>Logout</Button>
        </Form>

       ) : 
       (<Form inline>
            <FormControl isInvalid={isInvalid} type="text" placeholder="Name" name="name" className="mr-sm-2" />
            <FormControl isInvalid={isInvalid} type="password" placeholder="Password" name="password" className="mr-sm-2" />
            <Button type="submit" variant="outline-success" onClick={login}>Login</Button>
            <Button type="submit" variant="outline-success" onClick={register}>Register</Button>
        </Form>)
    }
      
    </Navbar.Collapse>
  </Navbar>)
}

export default Header