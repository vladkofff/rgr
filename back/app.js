const express = require("express");
const cors = require("cors");

const passport = require('passport');
require("./modules/passport");

const routes = require("./routes");

const app = express();

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(routes);
app.use(express.static(__dirname + '/build'))
app.get('/*', (req, res) => res.sendFile(path.join(__dirname, 'build', 'index.html')))

module.exports = app;

