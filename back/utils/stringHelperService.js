const generateRandomString = (minLen) => {
    let x = "";
    while (x.length <= minLen){
        x = x + Math.random().toString(36).substring(7).substr(2);
    }
    return x
};

module.exports = {generateRandomString};