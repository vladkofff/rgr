const bcrypt = require("bcrypt");

const compare = async (encrypted, comparable) => {
    return bcrypt.compare(comparable, encrypted)
}

const encrypt = async (password) => {
    return bcrypt.hash(password, 10);
}

module.exports = { compare, encrypt }