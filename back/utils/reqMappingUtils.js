exports.getInputFromReq = (req, input) => {
    const inputObject = input.split(".").reduce((acc, cur) => {
        return acc[cur];
    }, req)

    return inputObject;
}

exports.setOutputToReq = (output, req, data) => {
    const outputObject = output.split(".").reduce((acc, cur) => {
        acc[cur] ? {} : acc[cur] = {};

        return acc[cur];
    }, req)

    Object.keys(data).forEach(key => {
        outputObject[key] = data[key];
    })
}
