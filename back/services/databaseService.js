exports.create = async (model, object, transaction) => {
    return model.create(object, {transaction})
}
exports.findOne = async (model, conditions, transaction) => {
    return model.findOne({where: conditions}, transaction)
}
exports.findMany = async (model, conditions, transaction) => {
    return model.findAll({where: conditions}, transaction)
}
exports.update = async (model, conditions, object, transaction) => {
    return model.update(object, {where: conditions, returning: true, transaction})
}
exports.delete = async (model, conditions, transaction) => {
    return model.destroy({where: conditions, transaction})
}