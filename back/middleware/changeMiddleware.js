const { getInputFromReq, setOutputToReq } = require("../utils/reqMappingUtils");

exports.map = (input, output, fn) => {
    return async (req, res, next) => {
        const data = getInputFromReq(req, input);
        
        setOutputToReq(output, req, fn(data));
        next()
    }
}
