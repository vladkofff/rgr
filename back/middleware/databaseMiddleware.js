const databaseService = require("../services/databaseService");

const { getInputFromReq, setOutputToReq } = require("../utils/reqMappingUtils");

exports.findOne = (model, input, output) => {
    return async (req, res, next) => {
        const restrictions = getInputFromReq(req, input);
        const response = await databaseService.findOne(model, restrictions.conditions);
        setOutputToReq(output, req, response.dataValues);
        next()
    }
}
exports.findMany = (model, input, output) => {
    return async (req, res, next) => {
        const restrictions = getInputFromReq(req, input);
        const response = await databaseService.findMany(model, restrictions.conditions);
        setOutputToReq(output, req, {[model.tableName]: response.map(item => ({ ...item.dataValues }))});
        next()
    }
}
exports.create = (model, input, output) => {
    return async (req, res, next) => {
        const data = getInputFromReq(req, input);
        const response = await databaseService.create(model, data.object);
        setOutputToReq(output, req, response.dataValues);
        next()
    }
}
exports.update = (model, input, output) => {
    return async (req, res, next) => {
        const data = getInputFromReq(req, input);
        const [, response] = await databaseService.update(model, data.conditions, data.object);
        setOutputToReq(output, req, {[model.tableName]: response.map(item => ({ ...item.dataValues }))});
        next()
    }
}
exports.delete = (model, input) => {
    return async (req, res, next) => {
        const data = getInputFromReq(req, input);
        const _ = await databaseService.delete(model, data.conditions);
        next()
    }
}
