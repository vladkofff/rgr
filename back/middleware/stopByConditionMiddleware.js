const { getInputFromReq } = require("../utils/reqMappingUtils");

exports.stopByCondition = (input, fnCondition, fnStatus, fnResponse) => {
    return async (req, res, next) => {
        const data = getInputFromReq(req, input);
        
        if (!fnCondition(data)) {
            next()
        } else {
            res.status(fnStatus(data)).json(fnResponse(data));
        }
    }
}