const jwt = require("jsonwebtoken");
const passport = require("passport");

const db = require("../db");
const databaseService = require("../services/databaseService");

const passwordUtil = require("../utils/passwords");

const type = {
    jwt: "jwt"
}

exports.getUserByAccessToken = async (req, res, next) => {
    const [callback, token] = req.header("Authorization") ? req.header("Authorization").split(" ") : ["", ""];
    req.body.access_token = token;

    if (!type[callback]) {
        next()
    } else {
        passport.authenticate(type[callback], {session: false}, (err, user, info) => {
            req.user = user;
            delete req.body.access_token;
            next();
        })
        (req, res, next);
    }
}

exports.register = async (req, res, next) => {
    try {
        const user = await databaseService.create(db.user, {...req.body, password: await passwordUtil.encrypt(req.body.password)});
        const accessToken = jwt.sign({id: user.id,}, process.env.JWTSECRET || 'your_jwt_secret');

        res.json({user: {...user.dataValues}, accessToken })
    } catch (e) {
        res.status(400).json({})
    }
}
exports.login = async (req, res, next) => {
    passport.authenticate('local', {session: false}, (err, user, info) => {
        if (err || !user) {
            return res.status(400).json({
                error: [{
                    message: info ? info.message : 'Login failed',
                    path: "password" 
                }],
            });
        }

        req.login(user, {session: false}, async (err) => {
            if (err) {
                res.send(err);
            }

            try {
                const accessToken = jwt.sign({id: user.id, }, process.env.JWTSECRET || 'your_jwt_secret');
                res.json({user: {...user.dataValues}, accessToken });
            } catch (e) {
                res.status(500);
                res.json({})
            }
        });
    })
    (req, res, next);

}
