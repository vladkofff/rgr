const { getInputFromReq } = require("../utils/reqMappingUtils");

exports.send = (input) => {
    return async (req, res, next) => {
        const data = getInputFromReq(req, input);

        res.json(data);
    }
}
