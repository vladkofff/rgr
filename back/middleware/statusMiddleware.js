const { getInputFromReq } = require("../utils/reqMappingUtils");

exports.status = (input, fn) => {
    return async (req, res, next) => {
        const data = getInputFromReq(req, input);

        res.status(fn(data))
        next()
    }
}
