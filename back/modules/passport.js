const passport    = require('passport');
const passportJWT = require("passport-jwt");

const db = require("../db");
const passwordUtil = require("../utils/passwords");
const databaseService = require("../services/databaseService")

const userService = {
    findOne: async (conditions) => {
        return databaseService.findOne(db.user, conditions);
    }
}

const ExtractJWT = passportJWT.ExtractJwt;

const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy   = passportJWT.Strategy;

passport.use(new LocalStrategy({
        usernameField: 'name',
        passwordField: 'password'
    },
    async function (name, password, cb) {
        try {
            let user = await userService.findOne({name});
            if (!user) {
                return cb(null, false, {message: 'Incorrect name or password.'});                
            } else if (await passwordUtil.compare(user.password, password)) {
                return cb(null, user, {
                    message: 'Logged In Successfully'
                });
            } else {
                return cb(null, false, {message: 'Incorrect name or password.'});
            }
        } catch (err) {
            return cb(err);
        }
    }
));

passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderWithScheme("jwt"),
        secretOrKey   : process.env.JWTSECRET || 'your_jwt_secret'
    },
    function (jwtPayload, cb) {
        if (jwtPayload.admin) {
            return cb(null, jwtPayload);
        }
        
        return userService.findOne({id: jwtPayload.id})
            .then(beekeeper => {
                return cb(null, beekeeper);
            })
            .catch(err => {
                return cb(err);
            });
    }
));