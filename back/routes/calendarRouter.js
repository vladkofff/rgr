const express = require("express");
const router = express.Router();

const db = require("../db");

const changeMiddleware = require("../middleware/changeMiddleware");
const sendMiddleware = require("../middleware/sendMiddleware");
const databaseMiddleware = require("../middleware/databaseMiddleware");
const authMiddleware = require("../middleware/authMiddleware");
const stopByConditionMiddleware = require("../middleware/stopByConditionMiddleware");

router.post("/", 
    authMiddleware.getUserByAccessToken,
    stopByConditionMiddleware.stopByCondition("user", (user) => !user.id, i => 403, i => ({})),
    changeMiddleware.map("user", "body", (user) => ({userId: user.id})),
    changeMiddleware.map("body", "body.object", (data) => ({...data})),
    databaseMiddleware.create(db.calendar, "body", "calendar"),
    sendMiddleware.send("calendar")
)

router.get("/user", 
    authMiddleware.getUserByAccessToken,
    stopByConditionMiddleware.stopByCondition("user", (user) => !user.id, i => 403, i => ({})),
    changeMiddleware.map("user", "body.conditions", (user) => ({userId: user.id})),
    databaseMiddleware.findMany(db.calendar, "body", "calendars"),
    sendMiddleware.send("calendars")
)

router.get("/", 
    databaseMiddleware.findMany(db.calendar, "body", "calendars"), 
    sendMiddleware.send("calendars"))

module.exports = router;