const express = require("express");
const router = express.Router();

const db = require("../db");

const changeMiddleware = require("../middleware/changeMiddleware");
const sendMiddleware = require("../middleware/sendMiddleware");
const databaseMiddleware = require("../middleware/databaseMiddleware");
const authMiddleware = require("../middleware/authMiddleware");

router.post("/", 
    authMiddleware.getUserByAccessToken,
    changeMiddleware.map("user", "body", (user) => ({userId: user.id})),
    changeMiddleware.map("body", "body.object", (data) => ({...data})),
    databaseMiddleware.create(db.event, "body", "event"),
    sendMiddleware.send("event")
)

router.get("/", 
    changeMiddleware.map("query", "query.conditions", (i) => ({...i})),
    databaseMiddleware.findMany(db.event, "query", "events"),
    sendMiddleware.send("events")
)

module.exports = router;