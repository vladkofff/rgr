const express = require("express");
const router = express.Router();

const signRouter = require("./signRouter")
const eventRouter = require("./eventRouter")
const calendarRouter = require("./calendarRouter")

router.use("/api/sign", signRouter)
router.use("/api/event", eventRouter)
router.use("/api/calendar", calendarRouter)

module.exports = router;