const express = require("express");
const router = express.Router();

const authMiddleware = require("../middleware/authMiddleware");
const sendMiddleware = require("../middleware/sendMiddleware");

router.post("/login", authMiddleware.login)
router.post("/register", authMiddleware.register)
router.get("/", authMiddleware.getUserByAccessToken, sendMiddleware.send("user"))

module.exports = router;