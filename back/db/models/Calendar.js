module.exports = (sequelize, Sequelize, User) => {
    const Calendar = sequelize.define(
        "calendar",
        {
            id: {
                primaryKey: true,
                type: Sequelize.INTEGER,
                autoIncrement: true
            },
            userId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false
            }
        },
    );

    Calendar.belongsTo(User, {
        foreignKey: "userId"
    });
    User.hasMany(Calendar);

    return Calendar;
};

