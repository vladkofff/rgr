module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define(
    "user",
    {
        id: {
            primaryKey: true,
            type: Sequelize.INTEGER,
            autoIncrement: true
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING
        },
    }, {
        indexes: [
          { fields: ['name'], unique: true }
        ]
      }
    );

    return User;
};