module.exports = (sequelize, Sequelize, Calendar) => {
    const Event = sequelize.define(
        "event",
        {
            id: {
                primaryKey: true,
                type: Sequelize.INTEGER,
                autoIncrement: true
            },
            calendarId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            date: {
                type: Sequelize.DATE,
                allowNull: false
            },
            description: {
                type: Sequelize.STRING,
                allowNull: false
            },
            link: {
                type: Sequelize.STRING,
                allowNull: false
            }
        },
    );

    Event.belongsTo(Calendar, {
        foreignKey: "calendarId"
    });
    Calendar.hasMany(Event);

    return Event;
};

