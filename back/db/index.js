const Sequelize = require('sequelize');

const User = require("./models/User");
const Calendar = require("./models/Calendar");
const Event = require("./models/Event");

const sequelize = new Sequelize(process.env.DBNAME, process.env.DBUSERNAME, process.env.DBPASSWORD, {
    dialect: process.env.DBDIALECT,
    storage: './database.sqlite',
    logging: false
  });


const user = User(sequelize, Sequelize);
const calendar = Calendar(sequelize, Sequelize, user);
const event = Event(sequelize, Sequelize, calendar);



if (process.env.DBISNEEDSYNC === "true") {    
    sequelize.sync({alter: true});
}


module.exports = {sequelize, Sequelize, user, calendar, event};